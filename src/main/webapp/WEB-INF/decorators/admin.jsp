<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ include file="/WEB-INF/commons/taglib.jsp" %>
<html>
<head>
    <title><dec:title/></title>
    <script src="https://cdn.tailwindcss.com"></script>
    <link href="https://fonts.googleapis.com/css2?family=Roboto:ital,wght@0,100;0,300;0,400;0,500;0,700;0,900;1,100;1,300;1,400;1,500;1,700;1,900&display=swap"
          rel="stylesheet">
    <script src="https://unpkg.com/alpinejs" defer></script>
    <script src="https://cdn.tailwindcss.com"></script>
    <script src="https://ajax.googleapis.com/ajax/libs/angularjs/1.8.2/angular.min.js"></script>
</head>
<body>
<jsp:include page="/WEB-INF/commons/admin/header.jsp"/>
<div class="flex pt-16 overflow-hidden bg-gray-50 dark:bg-gray-900">
    <jsp:include page="/WEB-INF/commons/admin/sidebar.jsp"/>
    <div id="main-content" class="relative w-full h-full overflow-y-auto bg-gray-50 lg:ml-64 dark:bg-gray-900">
        <%--<div class="flex pt-16 overflow-hidden bg-gray-50 dark:bg-gray-900">--%>
        <dec:body/>
    </div>
</div>

</body>
</html>
