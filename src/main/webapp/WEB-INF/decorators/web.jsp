<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ include file="/WEB-INF/commons/taglib.jsp" %>
<html ng-app="cartModule" ng-controller="cartController">
<head>
    <title><dec:title/></title>
    <script src="https://cdn.tailwindcss.com"></script>
    <script src="https://ajax.googleapis.com/ajax/libs/angularjs/1.8.2/angular.min.js"></script>
    <script src="https://code.jquery.com/jquery-3.7.0.js"></script>
    <link href="https://fonts.googleapis.com/css2?family=Roboto:ital,wght@0,100;0,300;0,400;0,500;0,700;0,900;1,100;1,300;1,400;1,500;1,700;1,900&display=swap"
          rel="stylesheet">
</head>
<body class="bg-[#f5f5f5] font-['Roboto']">
<jsp:include page="/WEB-INF/commons/web/header.jsp"/>
<dec:body/>
<jsp:include page="/WEB-INF/commons/web/footer.jsp"/>
<script>
    angular.module("cartModule", [])
        .controller("cartController", function ($scope, orderService) {
            if (getCookie("cart") != null) $scope.productCart = getCookie("cart");
            else $scope.productCart = [];

            $scope.order = {};

            $scope.addOrder = (email, soDienThoai, tongTien, productCart) => {
                const orderDetailList = productCart.map(i => {
                    return {
                        donGia: i.giaBan,
                        soLuong: i.quantity,
                        productVariant: {
                            id: i.id
                        }
                    };
                });
                $scope.order = {
                    email: email,
                    soDienThoai: soDienThoai,
                    tongTien: tongTien,
                    orderDetailList: orderDetailList
                }

                orderService.insert($scope.order)
                    .then(data => {
                        window.location.href = window.location.origin + "/orderSummaries?variant=" + data.id;
                    })
                    .catch(err => alert(err));
            }

            $scope.total = () => {
                let total = 0;
                $scope.productCart.forEach(item => {
                    total += item.giaBan * item.quantity;
                });
                return total;
            }

            $scope.addToCart = (id, ten, giaBan, quantity) => {
                var obj = {
                    id: id,
                    ten: ten,
                    giaBan: giaBan,
                    quantity: quantity,
                    $$hashKey: id
                };

                var existingProduct = $scope.productCart.find(i => i.$$hashKey === obj.$$hashKey);
                if (existingProduct) {
                    existingProduct.quantity += obj.quantity;
                } else {
                    $scope.productCart.push(obj);
                }

                setCookie("cart", $scope.productCart, 3, "/");
                location.reload();
            }

            $scope.removeItemCart = (obj) => {
                var index = $scope.productCart.indexOf(obj);
                if (index > -1) {
                    $scope.productCart.splice(index, 1);
                    setCookie("cart", $scope.productCart, 3, "/");
                    location.reload();
                }
            }

            $scope.increase = (obj) => {
                var index = $scope.productCart.indexOf(obj);
                if (index !== -1) {
                    $scope.productCart[index].quantity += 1;
                    setCookie("cart", $scope.productCart, 3, "/");
                }
            }

            $scope.decrease = (obj) => {
                var index = $scope.productCart.indexOf(obj);
                if (index !== -1 && $scope.productCart[index].quantity > 1) {
                    $scope.productCart[index].quantity -= 1;
                    setCookie("cart", $scope.productCart, 3, "/");
                }
            }

            function setCookie(name, value, days, path) {
                var d = new Date();
                d.setTime(d.getTime() + (days * 24 * 60 * 60 * 1000));
                var expires = "expires=" + d.toUTCString();
                var p = "path=" + path;
                document.cookie = name + "=" + encodeURIComponent(JSON.stringify(value)) + "; " + expires + "; " + p;
            }

            function getCookie(name) {
                var cookieName = name + "=";
                var decodedCookie = decodeURIComponent(document.cookie);
                var cookieArray = decodedCookie.split(";");

                for (var i = 0; i < cookieArray.length; i++) {
                    var cookie = cookieArray[i];
                    while (cookie.charAt(0) === " ") {
                        cookie = cookie.substring(1);
                    }
                    if (cookie.indexOf(cookieName) === 0) {
                        var cookieValue = cookie.substring(cookieName.length, cookie.length);
                        return JSON.parse(cookieValue);
                    }
                }

                return null;
            }

            $scope.search = (id) => {
                orderService.getById(id)
                    .then(data => {
                        window.location.href = window.location.origin + "/orderSummaries?variant=" + data.id;
                    })
                    .catch(err => alert(err));
            }
        })
        .factory("orderService", function ($http) {
            var services = {};

            services.insert = (order) =>
                $http.post("http://localhost:8080/api/v1/order", order)
                    .then(respons => respons.data);

            services.getById = (id) =>
                $http.get("http://localhost:8080/api/v1/order/" + id)
                    .then(respons => respons.data);

            return services;
        });
</script>
</body>
</html>