async function checkUser(data, url) {
    const response = await fetch(url, {
        method: "POST",
        headers: {
            "Content-Type": "application/json",
        },
        body: JSON.stringify(data),
    });
    return await response.json();
}

function setError(error, status, message) {
    error.style.display = status;
    error.innerText = message;
}