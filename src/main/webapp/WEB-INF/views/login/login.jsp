<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@include file="/WEB-INF/commons/taglib.jsp" %>
<sec:authorize access="hasAnyRole('ADMIN')">
    <c:redirect url="/admin"/>
</sec:authorize>
<html>
<head>
    <title>Login - Admin</title>
    <script src="https://cdn.tailwindcss.com"></script>
    <link rel="stylesheet" href="/views/login/styles.css">
</head>
<body>
<main class="bg-gray-50 dark:bg-gray-900">
    <div class="flex flex-col items-center justify-center px-6 mx-auto md:h-screen pt:mt-0 dark:bg-gray-900">
        <a href="../trang-chu"
           class="flex items-center justify-center mb-8 text-2xl font-semibold lg:mb-10 dark:text-white">
            <img src="/assets/images/scren.png" class="mr-4 h-11"
                 alt="FlowBite Logo">
            <span>BBook</span>
        </a>
        <div id="login_error" class="w-full max-w-xl p-6 space-y-8 sm:p-8 bg-white rounded-lg shadow dark:bg-gray-800"
             style="display: ${empty accessDeniedError ? 'none' : 'block'};">
            ${accessDeniedError}
        </div>
        <!-- Card -->
        <div class="w-full max-w-xl p-6 space-y-8 sm:p-8 bg-white rounded-lg shadow dark:bg-gray-800">
            <h2 class="text-2xl font-bold text-gray-900 dark:text-white">
                Sign in to platform
            </h2>
            <form:form id="login_form" class="mt-8 space-y-6" onsubmit="return false">
                <div>
                    <label for="email_input" class="block mb-2 text-sm font-medium text-gray-900 dark:text-white">Your
                        email</label>
                    <input id="email_input"
                           class="bg-gray-50 border border-gray-300 text-gray-900 sm:text-sm rounded-lg focus:ring-blue-500 focus:border-blue-500 block w-full p-2.5 dark:bg-gray-700 dark:border-gray-600 dark:placeholder-gray-400 dark:text-white dark:focus:ring-blue-500 dark:focus:border-blue-500"
                           type="email"
                           name="username"
                           placeholder="name@company.com"
                           required
                    >
                </div>
                <div>
                    <label for="pass_input" class="block mb-2 text-sm font-medium text-gray-900 dark:text-white">Your
                        password</label>
                    <input id="pass_input"
                           class="bg-gray-50 border border-gray-300 text-gray-900 sm:text-sm rounded-lg focus:ring-blue-500 focus:border-blue-500 block w-full p-2.5 dark:bg-gray-700 dark:border-gray-600 dark:placeholder-gray-400 dark:text-white dark:focus:ring-blue-500 dark:focus:border-blue-500"
                           type="password"
                           name="password"
                           placeholder="••••••••"
                           required
                    >
                </div>
                <div class="flex items-start">
                    <div class="flex items-center h-5">
                        <input id="remember" aria-describedby="remember" name="remember" type="checkbox"
                               class="w-4 h-4 border-gray-300 rounded bg-gray-50 focus:ring-3 focus:ring-blue-300 dark:focus:ring-blue-600 dark:ring-offset-gray-800 dark:bg-gray-700 dark:border-gray-600">
                    </div>
                    <div class="ml-3 text-sm">
                        <label for="remember" class="font-medium text-gray-900 dark:text-white">Remember me</label>
                    </div>
                    <a href="#" class="ml-auto text-sm text-blue-700 hover:underline dark:text-blue-500">Lost
                        Password?</a>
                </div>
                <button id="login_btn" type="submit" onclick="validate()"
                        class="w-full px-5 py-3 text-base font-medium text-center text-white bg-blue-700 rounded-lg hover:bg-blue-800 focus:ring-4 focus:ring-blue-300 sm:w-auto dark:bg-blue-600 dark:hover:bg-blue-700 dark:focus:ring-blue-800">
                    Login to your account
                </button>
                <div class="text-sm font-medium text-gray-500 dark:text-gray-400">
                    Not registered? <a href="/register" class="text-blue-700 hover:underline dark:text-blue-500">Create
                    account</a>
                </div>
            </form:form>
        </div>
    </div>
</main>
<script type="text/javascript" src="/assets/js/user.js"></script>
<script type="text/javascript" src="/views/login/script.js"></script>
</body>
</html>
