const form = document.querySelector('#login_form');
const error = document.querySelector('#login_error');
const btnLogin = document.querySelector('#login_btn');
const email = document.querySelector("#email_input");
const pass = document.querySelector("#pass_input");

async function validate() {
    if (!form.checkValidity()) return;

    const rs = await checkUser({email: email.value, matKhau: pass.value}, "http://localhost:8080/api/v1/user/login");
    if (!rs.status)
    {
        setError(error, "block", rs.message);
        return;
    }
    form.submit();
}