angular.module("orderService", []).factory("orderService", function ($http) {
    var services = {};

    services.getAll = () =>
        $http.get("http://localhost:8080/api/v1/orders").then(respons => respons.data);

    services.delete = (id) =>
        $http.delete("http://localhost:8080/api/v1/order/" + id)
            .then(respons => respons.data);

    return services;
});