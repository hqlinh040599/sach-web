angular.module("userService", []).factory("userService", function ($http) {
    var services = {};

    services.getAll = () =>
        $http.get("http://localhost:8080/api/v1/users").then(respons => respons.data);

    services.insert = (user) =>
        $http.post("http://localhost:8080/api/v1/user", user)
            .then(respons => respons.data)
            .catch(err => err.message);

    services.getByUrl = (url) =>
         $http.get("http://localhost:8080/api/v1/user/" + url)
             .then(respons => respons.data);

    services.delete = (id) =>
        $http.delete("http://localhost:8080/api/v1/user/" + id)
            .then(respons => respons.data);

    return services;
});