angular.module("productService", []).factory("productService", function ($http) {
    var services = {};

    services.getAll = () =>
        $http.get("http://localhost:8080/api/v1/products").then(respons => respons.data);

    services.insert = (product) =>
        $http.post("http://localhost:8080/api/v1/product", product)
            .then(respons => respons.data)
            .catch(err => err.message);

    services.getByUrl = (url) =>
         $http.get("http://localhost:8080/api/v1/product/" + url)
             .then(respons => respons.data);

    services.delete = (id) =>
        $http.delete("http://localhost:8080/api/v1/product/" + id)
            .then(respons => respons.data);

    return services;
});