const form = document.querySelector('#register_form');
const error = document.querySelector('#register_error');
const btnRegister = document.querySelector('#register_btn');
const email = document.querySelector("#email_input");
const pass = document.querySelector("#pass_input");
const passConfirm = document.querySelector("#pass-confirm_input");

async function validate() {
    if (!form.checkValidity()) return;

    if (!(pass.value === passConfirm.value)) {
        setError(error, "block", "Xác nhận lại mật khẩu không trùng khớp! Vui lòng kiểm tra lại.");
        return;
    }
    const rs = await checkUser({email: email.value, matKhau: pass.value}, "http://localhost:8080/api/v1/user/register");
    if (!rs.status)
    {
        setError(error, "block", rs.message);
        return;
    }
    form.submit();
}


