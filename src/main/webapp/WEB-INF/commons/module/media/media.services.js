mediaModule.factory("mediaService", function ($http) {
    var services = {};

    services.getAll = () => $http.get("http://localhost:8080/api/v1/medias").then(response => response.data);
    services.insert = (formData) => {
        return $http.post("http://localhost:8080/api/v1/media", formData, {
            transformRequest: angular.identity,
            headers: {
                'Content-Type': undefined
            }
        });
    }
    services.update = (media) => $http.update("http://localhost:8080/api/v1/media/" + media.id, media)

    services.delete = (id) => $http.delete("http://localhost:8080/api/v1/media/" + id)

    return services;
});