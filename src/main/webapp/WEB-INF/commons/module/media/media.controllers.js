const mediaModule = angular.module("mediaModule", []);
mediaModule.controller("mediaController", ["$scope", "$rootScope", "mediaService",
    function ($scope, $rootScope, mediaService) {
        $rootScope.media = {
            isMultiSelect: true,
            mediaFocused: {},
            data: [],
            selectItem: function (e, obj) {
                if (this.isMultiSelect && (e.ctrlKey || e.metaKey)) obj.isSelected = !obj.isSelected;
                else {
                    this.data.forEach(i => i.isSelected = false);
                    obj.isSelected = true;
                }
                this.mediaFocused = obj;
                this.mediaFocused.url = window.location.origin + "/assets/images/" + obj.name;
            }
        };
        $scope.toasts = [];
        $scope.fillData = () => mediaService.getAll().then(data => {
            $scope.media.data = data;
            $scope.media.data.forEach((element) => element.isSelected = false);
        });

        $scope.insert = (e, userId) => {
            var files = e.target.files;
            if (files.length <= 0) return;
            var formData = new FormData();
            Array.from(files).forEach((file) => formData.append("files", file))
            formData.append("userId", userId);
            mediaService.insert(formData).then(data => $scope.fillData());
        }

        $scope.update = () => mediaService.update(media).then(() => $scope.fillData());

        $scope.delete = () => {
            $scope.isModalVisible = false;
            $scope.media.data.forEach((element) => {
                if (element.isSelected)
                    mediaService.delete(element.id).then(() => {
                        $scope.toasts.push({
                            id: element.id,
                            message: "Media id " + element.id + "was deleted successfully!",
                            opacity: "opacity-0",
                            type: "toast-success"
                        });
                        const toast = $scope.toasts.find(obj => obj.id === element.id);
                        setTimeout(() => {
                            toast.opacity = "opacity-100";
                            $scope.$apply();
                        }, 1);
                        setTimeout(() => {
                            toast.opacity = "opacity-0";
                            $scope.$apply();
                            setTimeout(() => {
                                $scope.toasts.splice(0, 1);
                                $scope.$apply();
                            }, 1000);
                        }, 3000);

                        $scope.fillData();
                    });
            });
        }

        $scope.fillData();
    }]);