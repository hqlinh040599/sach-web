<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ include file="/WEB-INF/commons/taglib.jsp" %>
<div ng-app="mediaModule" ng-controller="mediaController">
    <h1 class="text-xl font-semibold text-gray-900 sm:text-2xl dark:text-white">Media
        Library
    </h1>
    <jsp:include page="/WEB-INF/commons/module/media/upload-media.jsp"/>

    <div class="grid grid-cols-[7fr_3fr] gap-4 md:divide-x md:divide-gray-100 dark:divide-gray-700">
        <div>
            <div class="flex items-center mb-4">
                <form class="m-0 sm:pr-3" action="#" method="GET">
                    <label for="products-search" class="sr-only">Search</label>
                    <div class="relative w-48 mt-1 sm:w-64 xl:w-96">
                        <input type="text" name="email" id="products-search" ng-model="keyword"
                               class="bg-gray-50 border border-gray-300 text-gray-900 sm:text-sm rounded-lg focus:ring-blue-500 focus:border-blue-500 block w-full p-2.5 dark:bg-gray-700 dark:border-gray-600 dark:placeholder-gray-400 dark:text-white dark:focus:ring-blue-500 dark:focus:border-blue-500"
                               placeholder="Search for medias">
                    </div>
                </form>
                <div ng-show="(media.data | filter:{isSelected: true}).length > 0" id="media__tool"
                     class="flex items-center w-full sm:justify-end">
                    <div class="flex pl-2 space-x-1">
                        <button
                                class="inline-flex justify-center p-1 text-gray-500 rounded cursor-pointer hover:text-gray-900 hover:bg-gray-100 dark:text-gray-400 dark:hover:bg-gray-700 dark:hover:text-white">
                            <svg class="w-6 h-6" fill="currentColor" viewBox="0 0 20 20"
                                 xmlns="http://www.w3.org/2000/svg">
                                <path fill-rule="evenodd"
                                      d="M11.49 3.17c-.38-1.56-2.6-1.56-2.98 0a1.532 1.532 0 01-2.286.948c-1.372-.836-2.942.734-2.106 2.106.54.886.061 2.042-.947 2.287-1.561.379-1.561 2.6 0 2.978a1.532 1.532 0 01.947 2.287c-.836 1.372.734 2.942 2.106 2.106a1.532 1.532 0 012.287.947c.379 1.561 2.6 1.561 2.978 0a1.533 1.533 0 012.287-.947c1.372.836 2.942-.734 2.106-2.106a1.533 1.533 0 01.947-2.287c1.561-.379 1.561-2.6 0-2.978a1.532 1.532 0 01-.947-2.287c.836-1.372-.734-2.942-2.106-2.106a1.532 1.532 0 01-2.287-.947zM10 13a3 3 0 100-6 3 3 0 000 6z"
                                      clip-rule="evenodd"></path>
                            </svg>
                        </button>
                        <button
                                ng-click="isModalVisible = (media.data | filter:{isSelected: true}).length > 0"
                                class="inline-flex justify-center p-1 text-gray-500 rounded cursor-pointer hover:text-gray-900 hover:bg-gray-100 dark:text-gray-400 dark:hover:bg-gray-700 dark:hover:text-white">
                            <svg class="w-6 h-6" fill="currentColor" viewBox="0 0 20 20"
                                 xmlns="http://www.w3.org/2000/svg">
                                <path fill-rule="evenodd"
                                      d="M9 2a1 1 0 00-.894.553L7.382 4H4a1 1 0 000 2v10a2 2 0 002 2h8a2 2 0 002-2V6a1 1 0 100-2h-3.382l-.724-1.447A1 1 0 0011 2H9zM7 8a1 1 0 012 0v6a1 1 0 11-2 0V8zm5-1a1 1 0 00-1 1v6a1 1 0 102 0V8a1 1 0 00-1-1z"
                                      clip-rule="evenodd"></path>
                            </svg>
                        </button>
                        <button
                                class="inline-flex justify-center p-1 text-gray-500 rounded cursor-pointer hover:text-gray-900 hover:bg-gray-100 dark:text-gray-400 dark:hover:bg-gray-700 dark:hover:text-white">
                            <svg class="w-6 h-6" fill="currentColor" viewBox="0 0 20 20"
                                 xmlns="http://www.w3.org/2000/svg">
                                <path d="M10 6a2 2 0 110-4 2 2 0 010 4zM10 12a2 2 0 110-4 2 2 0 010 4zM10 18a2 2 0 110-4 2 2 0 010 4z"></path>
                            </svg>
                        </button>
                    </div>
                </div>
            </div>
            <div class="grid grid-cols-7 gap-2">
                <lable class="relative cursor-pointer {{(media.data | filter:{isSelected: true}).length > 1 ? 'p-2' : ''}}"
                       ng-repeat="item in media.data | filter: keyword" ng-click="media.selectItem($event, item)">
                    <input ng-model="item.isSelected" ng-show="(media.data | filter:{isSelected: true}).length > 1"
                           class="absolute top-0 right-0 w-4 h-4 text-blue-600 bg-gray-100 border-gray-300 rounded dark:ring-offset-gray-800 dark:bg-gray-700 dark:border-gray-600"
                           type="checkbox">

                    <img class="media-item aspect-square rounded-lg object-cover {{media.mediaFocused.id === item.id ? 'outline outline-[#1D4ED8] outline-offset-2 outline-2' : ''}}"
                         ng-src="/assets/images/{{item.name}}">
                </lable>
            </div>
        </div>

        <div class="bg-white shadow overflow-hidden sm:rounded-lg">
            <div class="flex gap-2 px-4 py-5 sm:px-6">
                <div class="relative basis-1/2">
                    <img class="absolute rounded-lg object-contain h-full"
                         ng-src="/assets/images/{{media.mediaFocused.name}}">
                </div>
                <div class="overflow-hidden text-ellipsis">
                    <h3 class="text-lg leading-6 font-medium text-gray-900">
                        {{media.mediaFocused.name}}
                    </h3>
                    <a href="/assets/images/{{media.mediaFocused.name}}" target="_blank"
                       class="mt-1 max-w-2xl text-sm text-gray-500">
                        {{media.mediaFocused.url}}
                    </a>
                </div>
            </div>
            <div class="border-t border-gray-200">
                <dl>
                    <div class="bg-gray-50 px-4 py-5 sm:grid sm:grid-cols-3 sm:gap-4 sm:px-6">
                        <dt class="text-sm font-medium text-gray-500">
                            ID
                        </dt>
                        <dd class="mt-1 text-sm text-gray-900 sm:mt-0 sm:col-span-2">
                            {{media.mediaFocused.id}}
                        </dd>
                    </div>
                    <div class="bg-white px-4 py-5 sm:grid sm:grid-cols-3 sm:gap-4 sm:px-6">
                        <dt class="text-sm font-medium text-gray-500">
                            Best techno
                        </dt>
                        <dd class="mt-1 text-sm text-gray-900 sm:mt-0 sm:col-span-2">

                        </dd>
                    </div>
                    <div class="bg-gray-50 px-4 py-5 sm:grid sm:grid-cols-3 sm:gap-4 sm:px-6">
                        <dt class="text-sm font-medium text-gray-500">
                            Email address
                        </dt>
                        <dd class="mt-1 text-sm text-gray-900 sm:mt-0 sm:col-span-2">
                            m.poul@example.com
                        </dd>
                    </div>
                    <div class="bg-white px-4 py-5 sm:grid sm:grid-cols-3 sm:gap-4 sm:px-6">
                        <dt class="text-sm font-medium text-gray-500">
                            Salary
                        </dt>
                        <dd class="mt-1 text-sm text-gray-900 sm:mt-0 sm:col-span-2">
                            $10,000
                        </dd>
                    </div>
                    <div class="bg-gray-50 px-4 py-5 sm:grid sm:grid-cols-3 sm:gap-4 sm:px-6">
                        <dt class="text-sm font-medium text-gray-500">
                            About
                        </dt>
                        <dd class="mt-1 text-sm text-gray-900 sm:mt-0 sm:col-span-2">
                            To get social media testimonials like these, keep your customers engaged with your social
                            media accounts by posting regularly yourself
                        </dd>
                    </div>
                </dl>
            </div>
        </div>
    </div>
    <jsp:include page="/WEB-INF/commons/component/confirm.jsp">
        <jsp:param name="message" value="Do you wanna delete?"/>
    </jsp:include>
    <jsp:include page="/WEB-INF/commons/component/toast.jsp"/>

</div>
<script src="/commons/module/media/media.controllers.js"></script>
<script src="/commons/module/media/media.services.js"></script>