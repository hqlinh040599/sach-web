<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<header class="mb-4">
    <nav class="bg-white border-gray-200 px-4 lg:px-6 py-2.5 dark:bg-gray-800">
        <div class="grid grid-cols-[2fr_5fr_2fr] justify-between items-center mx-auto max-w-screen-xl">
            <a href="/trang-chu" class="flex items-center">
                <img src="/assets/images/scren.png"
                     class="mr-3 h-6 sm:h-9" alt="Flowbite Logo"/>
                <span class="self-center text-xl font-semibold whitespace-nowrap dark:text-white">BBook</span>
            </a>
            <form class="m-0">
                <label for="default-search"
                       class="mb-2 text-sm font-medium text-gray-900 sr-only dark:text-white">Search</label>
                <div class="relative">
                    <div class="absolute inset-y-0 left-0 flex items-center pl-3 pointer-events-none">
                        <svg class="w-4 h-4 text-gray-500 dark:text-gray-400" aria-hidden="true"
                             xmlns="http://www.w3.org/2000/svg" fill="none" viewBox="0 0 20 20">
                            <path stroke="currentColor" stroke-linecap="round" stroke-linejoin="round"
                                  stroke-width="2" d="m19 19-4-4m0-7A7 7 0 1 1 1 8a7 7 0 0 1 14 0Z"></path>
                        </svg>
                    </div>
                    <input ng-model="id" type="search" id="default-search"
                           class="block w-full p-2 pl-10 text-sm text-gray-900 border border-gray-300 rounded-lg bg-gray-50 focus:ring-blue-500 focus:border-blue-500 dark:bg-gray-700 dark:border-gray-600 dark:placeholder-gray-400 dark:text-white dark:focus:ring-blue-500 dark:focus:border-blue-500"
                           placeholder="Nhập từ khoá muốn tìm kiếm..." required>
                    <button ng-click="search(id)" type="submit"
                            class="text-white absolute right-1 bottom-[3px] bg-blue-700 hover:bg-blue-800 focus:ring-4 focus:outline-none focus:ring-blue-300 font-medium rounded-lg text-sm px-4 py-1.5 dark:bg-blue-600 dark:hover:bg-blue-700 dark:focus:ring-blue-800">
                        Tìm kiếm
                    </button>
                </div>
            </form>

            <div class="flex items-center justify-end lg:order-2">
                <a href="../login"
                   class="text-gray-800 dark:text-white hover:bg-gray-50 focus:ring-1 focus:ring-gray-300 font-medium rounded-lg text-sm px-4 lg:px-5 py-2 lg:py-2.5 mr-2 dark:hover:bg-gray-700 focus:outline-none dark:focus:ring-gray-800">Đăng
                    nhập</a>
                <a href="../register"
                   class="text-white bg-blue-700 hover:bg-blue-800 focus:ring-4 focus:ring-blue-300 font-medium rounded-lg text-sm px-4 lg:px-5 py-2 lg:py-2.5 mr-2 dark:bg-blue-600 dark:hover:bg-blue-700 focus:outline-none dark:focus:ring-blue-800">Đăng
                    ký</a>

                <div>
                    <div id="cart__icon" class="flex justify-center items-center cursor-pointer relative">
                        <div class="relative py-2">
                            <div class="t-0 absolute left-5">
                                <p
                                        class="flex h-2 w-2 items-center justify-center rounded-full bg-red-500 p-[10px] text-xs text-white">
                                    {{productCart.length > 0 ? productCart.length : "0"}}
                                </p>
                            </div>
                            <svg xmlns="http://www.w3.org/2000/svg" fill="none" viewBox="0 0 24 24"
                                 stroke-width="1.5" stroke="currentColor" class="file: mt-4 h-7 w-7">
                                <path stroke-linecap="round" stroke-linejoin="round"
                                      d="M2.25 3h1.386c.51 0 .955.343 1.087.835l.383 1.437M7.5 14.25a3 3 0 00-3 3h15.75m-12.75-3h11.218c1.121-2.3 2.1-4.684 2.924-7.138a60.114 60.114 0 00-16.536-1.84M7.5 14.25L5.106 5.272M6 20.25a.75.75 0 11-1.5 0 .75.75 0 011.5 0zm12.75 0a.75.75 0 11-1.5 0 .75.75 0 011.5 0z"></path>
                            </svg>
                        </div>
                    </div>
                    <div id="cart__modal" class="mx-auto max-w-md absolute top-[80px] right-0 hidden z-50">
                        <div class="rounded-lg bg-white shadow w-[400px]">
                            <div class="px-4 pt-6 pb-4">
                                <div class="flow-root">
                                    <ul class="-my-8">
                                        <li ng-repeat="c in productCart"
                                            class="flex flex-col space-y-3 py-6 text-left sm:flex-row sm:space-x-5 sm:space-y-0">
                                            <div class="shrink-0 relative">
                                                <span
                                                        class="absolute top-1 left-1 flex h-6 w-6 items-center justify-center rounded-full border bg-white text-sm font-medium text-gray-500 shadow sm:-top-2 sm:-right-2">{{$index + 1}}</span>
                                                <img class="h-20 w-20 max-w-full rounded-lg object-cover"
                                                     src="https://images.unsplash.com/photo-1588484628369-dd7a85bfdc38?ixlib=rb-1.2.1&ixid=MnwxMjA3fDB8MHxzZWFyY2h8MTh8fHNuZWFrZXJ8ZW58MHx8MHx8&auto=format&fit=crop&w=150&q=60"
                                                     alt=""/>
                                            </div>

                                            <div class="relative flex flex-1 flex-col justify-between">
                                                <div class="sm:col-gap-5 sm:grid sm:grid-cols-3">
                                                    <div class="pr-8 sm:pr-5">
                                                        <p class="text-base font-semibold text-gray-900">{{c.ten}}</p>
                                                    </div>

                                                    <div class="mt-4 flex items-end justify-between sm:mt-0 sm:items-start sm:justify-end gap-[5px]">
                                                        <button ng-click="decrease(c)" ng-disabled="c.quantity === 1" class="border rounded-md px-2">-</button>
                                                        <span class="font-semibold mx-2">{{c.quantity}}</span>
                                                        <button ng-click="increase(c)" class="border rounded-md px-2">+</button>
                                                    </div>

                                                    <div class="mt-4 flex items-end justify-between sm:mt-0 sm:items-start sm:justify-end">
                                                        <p class="shrink-0 w-20 text-base font-semibold text-gray-900 sm:order-2 sm:ml-8 sm:text-right">
                                                            {{c.giaBan}}</p>
                                                    </div>
                                                </div>

                                                <div ng-click="removeItemCart(c)"
                                                     class="absolute top-0 right-0 flex sm:bottom-0 sm:top-auto">
                                                    <button type="button"
                                                            class="flex rounded p-2 text-center text-gray-500 transition-all duration-200 ease-in-out focus:shadow hover:text-gray-900">
                                                        <svg class="h-5 w-5" xmlns="http://www.w3.org/2000/svg"
                                                             fill="none" viewBox="0 0 24 24" stroke="currentColor">
                                                            <path stroke-linecap="round" stroke-linejoin="round"
                                                                  stroke-width="2" d="M6 18L18 6M6 6l12 12" class="">
                                                            </path>
                                                        </svg>
                                                    </button>
                                                </div>
                                            </div>
                                        </li>
                                    </ul>
                                </div>

                                <!-- <hr class="mx-0 mt-6 mb-0 h-0 border-r-0 border-b-0 border-l-0 border-t border-solid border-gray-300" /> -->

                                <div class="mt-6 space-y-3 border-t border-b py-8">
                                    <div class="flex items-center justify-between">
                                        <p class="text-gray-400">Subtotal</p>
                                        <p class="text-lg font-semibold text-gray-900">$2399.00</p>
                                    </div>
                                    <div class="flex items-center justify-between">
                                        <p class="text-gray-400">Shipping</p>
                                        <p class="text-lg font-semibold text-gray-900">$8.00</p>
                                    </div>
                                </div>
                                <div class="mt-6 flex items-center justify-between">
                                    <p class="text-sm font-medium text-gray-900">Total</p>
                                    <p class="text-2xl font-semibold text-gray-900">{{total()}}<span
                                            class="text-lg font-normal text-gray-400">đ</span></p>
                                </div>

                                <a href="/checkout">
                                    <div class="mt-6 text-center">
                                        <button type="button"
                                                class="group inline-flex w-full items-center justify-center rounded-md bg-orange-500 px-6 py-4 text-lg font-semibold text-white transition-all duration-200 ease-in-out focus:shadow hover:bg-gray-800">
                                            Place Order
                                            <svg xmlns="http://www.w3.org/2000/svg"
                                                 class="group-hover:ml-8 ml-4 h-6 w-6 transition-all" fill="none"
                                                 viewBox="0 0 24 24" stroke="currentColor" stroke-width="2">
                                                <path stroke-linecap="round" stroke-linejoin="round"
                                                      d="M13 7l5 5m0 0l-5 5m5-5H6"></path>
                                            </svg>
                                        </button>
                                    </div>
                                </a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </nav>
</header>
<script>
    $(document).ready(() => {
        $("#cart__icon").click(() => {
            $("#cart__modal").toggle();
        })
    });
</script>