package com.springmvc.apis;

import com.springmvc.dtos.MediaDTO;
import com.springmvc.dtos.UserDTO;
import com.springmvc.entity.User;
import com.springmvc.services.IUserService;
import com.springmvc.utils.ResponseObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@Controller
@RequestMapping(path = "/api/v1")
public class UserAPI {
    @Autowired
    private IUserService userServices;

    @RequestMapping(path = "/users", method = RequestMethod.GET)
    @ResponseBody
    public List<UserDTO> getAll() {
        return userServices.getAll();
    }

    @RequestMapping(value = "/user/{id}", method = RequestMethod.GET)
    @ResponseBody
    public UserDTO getById(@PathVariable("id") int id) {
        return userServices.getById(id);
    }

    @RequestMapping(path = "/user", method = RequestMethod.POST)
    @ResponseBody
    public void upload(@RequestBody UserDTO user) {
        userServices.insert(user);
    }

    @RequestMapping(path = "/user/{id}", method = RequestMethod.DELETE)
    @ResponseBody
    public void delete(@PathVariable("id") Integer id) {
        userServices.delete(id);
    }

    @RequestMapping(path = "/user/login", method = RequestMethod.POST)
    @ResponseBody
    public ResponseObject<UserDTO> checkAuth(@RequestBody UserDTO user) {
        UserDTO data = null;
        try {
            data = userServices.checkAuth(user);
            return new ResponseObject<UserDTO>(data, "Thành công.", true);
        } catch (IllegalStateException e) {
            return new ResponseObject<UserDTO>(null, e.getMessage(), false);
        }
    }

    @RequestMapping(path = "/user/register", method = RequestMethod.POST)
    @ResponseBody
    public ResponseObject<UserDTO> checkRegister(@RequestBody UserDTO user) {
        UserDTO data = null;
        try {
            data = userServices.checkRegister(user);
            return new ResponseObject<>(data, "Thành công.", true);
        } catch (IllegalStateException e) {
            return new ResponseObject<>(null, e.getMessage(), false);
        }
    }

}
