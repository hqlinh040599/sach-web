package com.springmvc.apis;

import com.springmvc.dtos.OrderDTO;
import com.springmvc.services.IOrderService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.config.annotation.EnableWebMvc;

import java.util.List;

@Controller
@EnableWebMvc
@RequestMapping(value = "/api/v1")
public class OrderAPI {
    @Autowired
    private IOrderService orderServices;

    @RequestMapping(value = "/orders", method = RequestMethod.GET)
    @ResponseBody
    public List<OrderDTO> getAll() {
        return orderServices.getAll();
    }

    @RequestMapping(value = "/order/{id}", method = RequestMethod.GET)
    @ResponseBody
    public OrderDTO getByPath(@PathVariable("id") String id) {
        System.out.println(orderServices.getById(Integer.parseInt(id)));
        return orderServices.getById(Integer.parseInt(id));
    }

    @RequestMapping(value = "/order", method = RequestMethod.POST)
    @ResponseBody
    public OrderDTO insert(@RequestBody OrderDTO orderDTO) {
        return orderServices.insert(orderDTO);
    }

    @RequestMapping(value = "/order/{id}", method = RequestMethod.PUT)
    @ResponseBody
    public void update(@RequestBody OrderDTO orderDTO, @PathVariable("id") Integer id) {
        orderServices.update(orderDTO);
    }

    @RequestMapping(value = "/order/{id}", method = RequestMethod.DELETE)
    @ResponseBody
    public void delete(@PathVariable("id") int id) {
        orderServices.delete(id);
    }
}
