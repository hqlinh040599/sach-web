package com.springmvc.apis;

import com.springmvc.dtos.ProductDTO;
import com.springmvc.dtos.ProductVariantDTO;
import com.springmvc.services.IProductVariantService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.config.annotation.EnableWebMvc;

@Controller
@EnableWebMvc
@RequestMapping(value = "/api/v1")
public class ProductVariantAPI {
    @Autowired
    private IProductVariantService productVariantService;

    @RequestMapping(value = "/productvariant", method = RequestMethod.POST)
    @ResponseBody
    public ProductVariantDTO insert(@RequestBody ProductVariantDTO productVariantDTO) {
        return productVariantService.insert(productVariantDTO);
    }
}
