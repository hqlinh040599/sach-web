package com.springmvc.apis;

import com.springmvc.dtos.MediaDTO;
import com.springmvc.dtos.UserDTO;
import com.springmvc.entity.Media;
import com.springmvc.repositories.IMediaRepository;
import com.springmvc.services.IMediaService;
import com.springmvc.utils.ResponseObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.config.annotation.EnableWebMvc;

import javax.servlet.http.HttpServletRequest;
import java.io.File;
import java.util.Arrays;
import java.util.List;

@Controller
@RequestMapping(value = "/api/v1")
public class MediaAPI {
    @Autowired
    private IMediaService mediaService;

    @RequestMapping(value = "/medias", method = RequestMethod.GET)
    @ResponseBody
    public List<MediaDTO> getMedias(@RequestParam(value = "vaiTroId", required = false) Integer vaiTroId,
                                    @RequestParam(value = "taiKhoanId", required = false) Integer taiKhoanId) {
        if (vaiTroId != null) return mediaService.getByTaiKhoanVaiTroId(vaiTroId);
        else if (taiKhoanId != null) return mediaService.getByTaiKhoanId(taiKhoanId);
        return mediaService.getAll();
    }

    @RequestMapping(value = "/media/{id}", method = RequestMethod.GET)
    @ResponseBody
    public MediaDTO getById(@PathVariable("id") int id) {
        return mediaService.getById(id);
    }

    @RequestMapping(value = "/media", method = RequestMethod.POST)
    @ResponseBody
    public void insert(HttpServletRequest request, @RequestParam("files") MultipartFile[] files, @RequestParam("userId") Integer userId) {
        if (files[0].getSize() > 0) {
            Arrays.stream(files).forEach(i -> {
                try {
                    String uploadDir = request.getSession().getServletContext().getRealPath("/WEB-INF/assets/images");
                    MediaDTO mediaDTO = new MediaDTO();
                    UserDTO userDTO = new UserDTO();
                    userDTO.setId(userId);
                    mediaDTO.setUser(userDTO);
                    mediaDTO.setName(i.getOriginalFilename());
                    MediaDTO mdto = mediaService.insert(mediaDTO);
                    if (mdto != null) {
                        String filePath = uploadDir + File.separator + mdto.getName();
                        File dest = new File(filePath);
                        i.transferTo(dest);
                    }
                } catch (Exception e) {
                    System.out.println(e.getMessage());
                }
            });
        }
    }

    @RequestMapping(value = "/media/{id}", method = RequestMethod.DELETE)
    @ResponseBody
    public void delete(@PathVariable("id") Integer id) {
        mediaService.delete(id);
    }
}
