package com.springmvc.apis;

import com.springmvc.dtos.RoleDTO;
import com.springmvc.entity.Role;
import com.springmvc.services.IRoleService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.config.annotation.EnableWebMvc;

import java.util.List;

@Controller
@RequestMapping(value = "/api/v1")
public class RoleAPI {
    @Autowired
    private IRoleService roleServices;

    @RequestMapping(value = "/roles", method = RequestMethod.GET)
    @ResponseBody
    public List<RoleDTO> getAll() {
        return roleServices.getAll();
    }

    @RequestMapping(value = "/role/{id}", method = RequestMethod.GET)
    @ResponseBody
    public RoleDTO getById(@PathVariable("id") int id) {
        return roleServices.getById(id);
    }

    @RequestMapping(value = "/role", method = RequestMethod.POST)
    public String insert(@RequestBody RoleDTO role) {
        roleServices.insert(role);
        return "redirect:/api/v1/roles";
    }

    @RequestMapping(value = "/role/{id}", method = RequestMethod.DELETE)
    public void delete(@PathVariable("id") Integer id) {
        roleServices.delete(id);
    }
}
