package com.springmvc.apis;

import com.springmvc.dtos.ProductDTO;
import com.springmvc.entity.Product;
import com.springmvc.services.IProductService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.config.annotation.EnableWebMvc;

import java.util.List;

@Controller
@EnableWebMvc
@RequestMapping(value = "/api/v1")
public class ProductAPI {
    @Autowired
    private IProductService productServices;

    @RequestMapping(value = "/products", method = RequestMethod.GET)
    @ResponseBody
    public List<ProductDTO> getAll() {
        return productServices.getAll();
    }

    @RequestMapping(value = "/product/{path}", method = RequestMethod.GET)
    @ResponseBody
    public ProductDTO getByPath(@PathVariable("path") String path) {
        ProductDTO productDTO = null;
        try {
            productDTO = productServices.getById(Integer.parseInt(path));
        } catch (Exception e) {
            productDTO = productServices.getByUrl(path);
        }
        return productDTO;
    }

    @RequestMapping(value = "/product", method = RequestMethod.POST)
    @ResponseBody
    public ProductDTO insert(@RequestBody ProductDTO product) {
        return productServices.insert(product);
    }

    @RequestMapping(value = "/product/{id}", method = RequestMethod.PUT)
    @ResponseBody
    public void update(@RequestBody ProductDTO product, @PathVariable("id") Integer id) {
        productServices.update(product);
    }

    @RequestMapping(value = "/product/{id}", method = RequestMethod.DELETE)
    @ResponseBody
    public void delete(@PathVariable("id") int id) {
        productServices.delete(id);
    }
}
