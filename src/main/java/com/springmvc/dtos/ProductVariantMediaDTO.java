package com.springmvc.dtos;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
public class ProductVariantMediaDTO extends AbstractDTO {
    private ProductVariantDTO productVariant;
    private MediaDTO media;
}
