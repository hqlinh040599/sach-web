package com.springmvc.dtos;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.*;

@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
public class MediaDTO extends AbstractDTO {
    private String name;
    private String alt;
    @JsonIgnoreProperties(value = "avatar")
    private UserDTO user;
}
