package com.springmvc.dtos;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.databind.annotation.JsonAppend;
import com.springmvc.utils.CurrencyUtils;
import lombok.*;

import java.util.List;

@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
public class ProductDTO extends AbstractDTO{
    private String tenSanPham;
    private String moTaNgan;
    private Boolean trangThai = true;
    private String url;
    private MediaDTO hinhAnh;
    @JsonIgnoreProperties(value = "product")
    private List<ProductVariantDTO> productVariantList;

    public String getMinPrice() {
        return CurrencyUtils.parseString(productVariantList.stream().mapToLong(ProductVariantDTO::getGiaBan).min().getAsLong());
    }
    public String getMaxPrice() {
        return CurrencyUtils.parseString(productVariantList.stream().mapToLong(ProductVariantDTO::getGiaBan).max().getAsLong());
    }
}

