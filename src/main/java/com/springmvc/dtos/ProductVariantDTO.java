package com.springmvc.dtos;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.springmvc.entity.ProductVariant;
import com.springmvc.utils.CurrencyUtils;
import lombok.*;

import java.util.List;

@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
public class ProductVariantDTO extends AbstractDTO {
    private String tenBienThe;
    private String chiTietMoTa;
    private int soTrang;
    private Boolean trangThai = true;
    private int giaBan;
    private ProductDTO product;
    private List<MediaDTO> mediaDTOList;

    public String getGiaVN() {
        return CurrencyUtils.parseString(giaBan);
    }
}
