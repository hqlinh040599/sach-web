package com.springmvc.dtos;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.*;

import java.util.Date;

@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
public class UserDTO extends AbstractDTO {
    private String tenTaiKhoan;
    private String matKhau;
    private String hoTen;
    private String email;
    private String soDienThoai;
    private Date ngayTao;
    @JsonIgnoreProperties(value = "user")
    private MediaDTO avatar;
    private RoleDTO role;
    private AddressDTO address;
}
