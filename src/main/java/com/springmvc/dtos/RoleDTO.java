package com.springmvc.dtos;

import lombok.*;

@AllArgsConstructor
@NoArgsConstructor
@Data
public class RoleDTO extends AbstractDTO {
    private String tenVaiTro;
    private String moTa;
}
