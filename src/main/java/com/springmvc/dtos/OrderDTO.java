package com.springmvc.dtos;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.*;

import java.sql.Timestamp;
import java.time.LocalDateTime;
import java.util.Date;
import java.util.List;

@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
public class OrderDTO extends AbstractDTO {
    private String email;
    private String soDienThoai;
    private long tongTien;
    private Timestamp ngayTao;
    @JsonIgnoreProperties(value = "order")
    private List<OrderDetailDTO> orderDetailList;
}