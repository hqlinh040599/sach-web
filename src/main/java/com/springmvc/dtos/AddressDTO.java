package com.springmvc.dtos;

import lombok.*;

@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
public class AddressDTO extends AbstractDTO {
    private String diaChi;
    private float kinhDo;
    private float viDo;
    private UserDTO user;
    private CityDTO city;
}
