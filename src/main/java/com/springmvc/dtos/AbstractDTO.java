package com.springmvc.dtos;

import lombok.Data;

@Data
public abstract class AbstractDTO {
    protected int id;
}
