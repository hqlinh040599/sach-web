package com.springmvc.dtos;

import lombok.*;

@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
public class CityDTO extends AbstractDTO{
    private String tenThanhPho;
}
