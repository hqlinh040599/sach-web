package com.springmvc.services;

import com.springmvc.dtos.UserDTO;

public interface IUserService extends ICrudService<UserDTO> {
    public UserDTO checkAuth(UserDTO user);
    public UserDTO checkRegister(UserDTO user);
    public UserDTO getByTenTaiKhoan(String username);
}
