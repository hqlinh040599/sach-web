package com.springmvc.services.impl;

import com.springmvc.dtos.OrderDTO;
import com.springmvc.dtos.OrderDetailDTO;
import com.springmvc.dtos.ProductDTO;
import com.springmvc.dtos.ProductVariantDTO;
import com.springmvc.entity.Order;
import com.springmvc.entity.OrderDetail;
import com.springmvc.entity.Product;
import com.springmvc.entity.ProductVariant;
import com.springmvc.repositories.IOrderDetailRepository;
import com.springmvc.repositories.IOrderRepository;
import com.springmvc.repositories.IProductVariantRepository;
import com.springmvc.services.IOrderService;
import com.springmvc.utils.DTOUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.sql.Timestamp;
import java.util.Date;
import java.util.List;

@Service
@Transactional
public class OrderService implements IOrderService {
    private @Autowired IOrderRepository orderRepository;
    private @Autowired IOrderDetailRepository orderDetailRepository;
    private @Autowired IProductVariantRepository productVariantRepository;
    @Override
    public List<OrderDTO> getAll() {
        return DTOUtils.mapList(orderRepository.findAll(), OrderDTO.class);
    }

    @Override
    public OrderDTO getById(Integer id) {
        Order order = orderRepository.findById(id).orElseThrow(() -> new IllegalStateException("không tìm thấy đơn hàng có id là " + id));
        return DTOUtils.map(order, OrderDTO.class);
    }

    @Override
    public OrderDTO insert(OrderDTO orderDTO) {
        orderDTO.setNgayTao(new Timestamp(new Date().getTime()));
        Order orderEntity = orderRepository.save(DTOUtils.map(orderDTO, Order.class));
        OrderDTO dto = DTOUtils.map(orderEntity, OrderDTO.class);
        orderDTO.getOrderDetailList().forEach(i -> {
            OrderDetail orderDetail = DTOUtils.map(i, OrderDetail.class);
            ProductVariant productVariantEntity = productVariantRepository.findById(i.getProductVariant().getId());
            orderDetail.setProductVariant(productVariantEntity);
            orderDetail.setOrder(orderEntity);
            orderDetailRepository.save(orderDetail);
        });
        dto.setOrderDetailList(DTOUtils.mapList(orderDetailRepository.findAllByOrder(orderEntity), OrderDetailDTO.class));
        return dto;
    }

    @Override
    public void update(OrderDTO orderDTO) {
        System.out.println(orderDTO);
//        Order order = DTOUtils.map(getById(orderDTO.getId()), Order.class);
//        order.setEmail(orderDTO.getEmail());
//        order.setSoDienThoai(orderDTO.getSoDienThoai());
//        order.setTongTien(orderDTO.getTongTien());
//        order.setNgayTao(orderDTO.getNgayTao());
//        order.setOrderDetailList(DTOUtils.mapList(orderDTO.getOrderDetailList(), OrderDetail.class));
//        orderRepository.save(order);
    }

    @Override
    public void delete(Integer id) {
        orderRepository.deleteById(id);
    }
}
