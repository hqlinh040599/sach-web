package com.springmvc.services.impl;

import com.springmvc.dtos.RoleDTO;
import com.springmvc.repositories.IRoleRepository;
import com.springmvc.services.IRoleService;
import com.springmvc.utils.DTOUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Service
@Transactional
public class RoleService implements IRoleService {

    @Autowired
    private IRoleRepository roleRepositories;

    @Override
    public List<RoleDTO> getAll() {
        return DTOUtils.mapList(roleRepositories.findAll(), RoleDTO.class);
    }

    @Override
    public RoleDTO getById(Integer id) {
        return DTOUtils.map(roleRepositories.findById(id).orElseThrow(() -> new IllegalStateException("không tìm thấy quyền với id " + id)), RoleDTO.class);
    }

    @Override
    public RoleDTO insert(RoleDTO t) {
return null;
    }

    @Override
    public void update(RoleDTO t) {
        RoleDTO role = getById(t.getId());
    }

    @Override
    public void delete(Integer id) {
        boolean exist = roleRepositories.existsById(id);
        if (!exist) throw new IllegalStateException("phân quyền với id " + id + " không tồn tại!");
        roleRepositories.deleteById(id);
    }
}
