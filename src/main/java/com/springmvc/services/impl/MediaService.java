package com.springmvc.services.impl;

import com.springmvc.dtos.MediaDTO;
import com.springmvc.dtos.RoleDTO;
import com.springmvc.dtos.UserDTO;
import com.springmvc.entity.Media;
import com.springmvc.entity.User;
import com.springmvc.entity.Role;
import com.springmvc.repositories.IMediaRepository;
import com.springmvc.services.IMediaService;
import com.springmvc.services.IRoleService;
import com.springmvc.services.IUserService;
import com.springmvc.utils.DTOUtils;
import org.apache.commons.io.FileUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.List;

@Service
@Transactional
public class MediaService implements IMediaService {
    private @Autowired IMediaRepository mediaRepository;
    private @Autowired IRoleService roleService;
    private @Autowired IUserService userService;

    @Override
    public List<MediaDTO> getAll() {
        return DTOUtils.mapList(mediaRepository.findAll(), MediaDTO.class);
    }

    @Override
    public MediaDTO getById(Integer id) {
        return DTOUtils.map(mediaRepository.findById(id).orElseThrow(() -> new IllegalStateException("không tìm thấy hình ảnh có id " + id)), MediaDTO.class);
    }

    @Override
    public MediaDTO insert(MediaDTO media) {
        String newName = media.getName();
        int count = 1;
        while (mediaRepository.existsAllByName(newName)) {
            int dotIndex = media.getName().lastIndexOf(".");
            String name = media.getName().substring(0, dotIndex);
            String extension = media.getName().substring(dotIndex);
            newName = name + "-" + count + extension;
            count++;
        }
        media.setName(newName);
        int idTK = media.getUser().getId();
        UserDTO userDTO = userService.getById(idTK);
        media.setUser(userDTO);
        return DTOUtils.map(mediaRepository.save(DTOUtils.map(media, Media.class)), MediaDTO.class);
    }

    @Override
    public void update(MediaDTO media) {

    }

    @Override
    public void delete(Integer id) {
        MediaDTO mediaDTO = getById(id);
        mediaRepository.delete(DTOUtils.map(mediaDTO, Media.class));
        try {
            Files.deleteIfExists(Paths.get("src/main/webapp/WEB-INF/assets/images/" + mediaDTO.getName()));
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }

    @Override
    public List<MediaDTO> getByTaiKhoanId(int id) {
        UserDTO user = userService.getById(id);
        return DTOUtils.mapList(mediaRepository.findByUser(DTOUtils.map(user, User.class)), MediaDTO.class);
    }

    @Override
    public List<MediaDTO> getByTaiKhoanVaiTroId(int id) {
        RoleDTO role = roleService.getById(id);
        return DTOUtils.mapList(mediaRepository.findByUserRole(DTOUtils.map(role, Role.class)), MediaDTO.class);
    }

    @Override
    public List<MediaDTO> getByName(String name) {
        return DTOUtils.mapList(mediaRepository.findByName(name), MediaDTO.class);
    }
}
