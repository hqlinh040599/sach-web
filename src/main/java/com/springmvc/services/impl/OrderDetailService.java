package com.springmvc.services.impl;

import com.springmvc.dtos.OrderDetailDTO;
import com.springmvc.repositories.IOrderDetailRepository;
import com.springmvc.services.IOrderDetailService;
import com.springmvc.utils.DTOUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Service
@Transactional
public class OrderDetailService implements IOrderDetailService {
    private @Autowired IOrderDetailRepository orderDetailRepository;
    @Override
    public List<OrderDetailDTO> getAll() {
        return DTOUtils.mapList(orderDetailRepository.findAll(), OrderDetailDTO.class);
    }

    @Override
    public OrderDetailDTO getById(Integer id) {
        return null;
    }

    @Override
    public OrderDetailDTO insert(OrderDetailDTO orderDetailDTO) {
        return null;
    }

    @Override
    public void update(OrderDetailDTO orderDetailDTO) {

    }

    @Override
    public void delete(Integer id) {

    }
}
