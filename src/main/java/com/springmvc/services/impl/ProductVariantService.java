package com.springmvc.services.impl;

import com.springmvc.dtos.ProductDTO;
import com.springmvc.dtos.ProductVariantDTO;
import com.springmvc.dtos.ProductVariantMediaDTO;
import com.springmvc.entity.Product;
import com.springmvc.entity.ProductVariant;
import com.springmvc.repositories.IProductVariantRepository;
import com.springmvc.services.IProductVariantService;
import com.springmvc.utils.DTOUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Service
@Transactional
public class ProductVariantService implements IProductVariantService {
    private @Autowired IProductVariantRepository productVariantRepository;

    @Override
    public List<ProductVariantDTO> getAll() {
        return DTOUtils.mapList(productVariantRepository.findAll(), ProductVariantDTO.class);
    }

    @Override
    public ProductVariantDTO getById(Integer id) {
        ProductVariant productVariant = productVariantRepository.findById(id).orElseThrow(() -> new IllegalStateException("không tìm thấy sản phẩm chi tiết có id là " + id));
        ProductVariantDTO productVariantDTO = DTOUtils.map(productVariant, ProductVariantDTO.class);
        return productVariantDTO;
    }

    @Override
    public ProductVariantDTO insert(ProductVariantDTO productVariant) {
        ProductVariantDTO productVariantDTO = DTOUtils.map(productVariantRepository.save(DTOUtils.map(productVariant, ProductVariant.class)), ProductVariantDTO.class);
        return productVariantDTO;
    }

    @Override
    public void update(ProductVariantDTO productVariant) {
        productVariantRepository.save(DTOUtils.map(productVariant, ProductVariant.class));
    }

    @Override
    public void delete(Integer id) {
        productVariantRepository.deleteById(id);
    }

    @Override
    public ProductVariantDTO getByProductUrl(String url, Integer id) {
        return null;
    }

    @Override
    public List<ProductVariantDTO> getAllByProduct(ProductDTO dto) {
        List<ProductVariant> productVariantList = productVariantRepository.findAllByProduct(DTOUtils.map(dto, Product.class));
        if (productVariantList == null)
            throw new IllegalStateException("Không tìm thấy biến thể nào trong sản phẩm " + dto.getTenSanPham());
        return DTOUtils.mapList(productVariantList, ProductVariantDTO.class);
    }

    @Override
    public void deleteByProduct(ProductDTO productDTO) {
        productVariantRepository.deleteAllByProduct(DTOUtils.map(productDTO, Product.class));
    }
}
