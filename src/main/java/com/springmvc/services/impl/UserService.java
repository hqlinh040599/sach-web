package com.springmvc.services.impl;

import com.springmvc.dtos.RoleDTO;
import com.springmvc.dtos.UserDTO;
import com.springmvc.entity.User;
import com.springmvc.repositories.IUserRepository;
import com.springmvc.services.IRoleService;
import com.springmvc.services.IUserService;
import com.springmvc.utils.DTOUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Optional;

@Service
@Transactional
public class UserService implements IUserService, UserDetailsService {
    @Autowired
    private IUserRepository userRepositories;
    @Autowired
    private IRoleService roleService;
    @Autowired
    private PasswordEncoder passwordEncoder;

    @Override
    public List<UserDTO> getAll() {
        return DTOUtils.mapList(userRepositories.findAll(), UserDTO.class);
    }

    @Override
    public UserDTO getById(Integer id) {
        return DTOUtils.map(userRepositories.findById(id).orElseThrow(() -> new IllegalStateException("không tìm thấy tài khoản có id " + id)), UserDTO.class);
    }

    @Override
    public UserDTO checkAuth(UserDTO user) {
        String errorMessage = "Sai tên tài khoản hoặc mật khẩu!";
        User tk = userRepositories.findByEmail(user.getEmail()).orElseThrow(() -> new IllegalStateException(errorMessage));
        if (!passwordEncoder.matches(user.getMatKhau(), tk.getPassword()))
            throw new IllegalStateException(errorMessage);
        return DTOUtils.map(tk, UserDTO.class);
    }

    @Override
    public UserDTO checkRegister(UserDTO user) {
        if (user.getEmail() == null)
            throw new IllegalStateException("email không được phép để trống!");

        Optional<User> taiKhoanByEmail = userRepositories.findByEmail(user.getEmail());
        if (user.getEmail() != null && taiKhoanByEmail.isPresent())
            throw new IllegalStateException("email đã tồn tại!");

        if (user.getTenTaiKhoan() != null) {
            Optional<User> taiKhoanByTTK = userRepositories.findByTenTaiKhoan(user.getTenTaiKhoan());
            if (taiKhoanByTTK.isPresent())
                throw new IllegalStateException("tên tài khoản đã tồn tại!");
        }

        RoleDTO role = roleService.getById(2);
        user.setRole(role);

        user.setMatKhau(passwordEncoder.encode(user.getMatKhau()));
        return user;
    }

    @Override
    public UserDTO getByTenTaiKhoan(String username) {
        return DTOUtils.map(userRepositories.findByTenTaiKhoan(username).orElseThrow(() -> new IllegalStateException("không tìm thấy tài khoản có username " + username)), UserDTO.class);
    }

    @Override
    public UserDTO insert(UserDTO user) {
        return DTOUtils.map(userRepositories.save(DTOUtils.map(checkRegister(user), User.class)), UserDTO.class);
    }

    @Override
    public void update(UserDTO user) {
        UserDTO tk = getById(user.getId());
        if (!user.getTenTaiKhoan().isEmpty()) tk.setTenTaiKhoan(user.getTenTaiKhoan());
        if (!user.getMatKhau().isEmpty()) tk.setMatKhau(user.getMatKhau());
        if (user.getRole() != null) tk.setRole(user.getRole());
        userRepositories.save(DTOUtils.map(tk, User.class));
    }

    @Override
    public void delete(Integer id) {
        boolean exist = userRepositories.existsById(id);
        if (!exist) throw new IllegalStateException("tài khoản với id " + id + " không tồn tại!");
        userRepositories.deleteById(id);
    }

    @Override
    public UserDetails loadUserByUsername(String s) throws UsernameNotFoundException {
        return userRepositories.findByEmail(s).orElseThrow(() -> new IllegalStateException("không tìm thấy tài khoản có email " + s));
    }
}
