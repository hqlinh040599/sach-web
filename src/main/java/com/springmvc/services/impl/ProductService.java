package com.springmvc.services.impl;

import com.springmvc.dtos.ProductDTO;
import com.springmvc.entity.Product;
import com.springmvc.repositories.IProductRepository;
import com.springmvc.services.IProductService;
import com.springmvc.services.IProductVariantService;
import com.springmvc.utils.DTOUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Service
@Transactional
public class ProductService implements IProductService {
    private @Autowired IProductRepository productRepositories;
    private @Autowired IProductVariantService productVariantService;

    @Override
    public List<ProductDTO> getAll() {
        return DTOUtils.mapList(productRepositories.findAll(), ProductDTO.class);
    }

    @Override
    public ProductDTO getById(Integer id) {
        Product product = productRepositories.findById(id).orElseThrow(() -> new IllegalStateException("không tìm thấy sản phẩm có id là " + id));
        return DTOUtils.map(product, ProductDTO.class);
    }

    @Override
    public ProductDTO insert(ProductDTO p) {
        Product product = productRepositories.save(DTOUtils.map(p, Product.class));
        ProductDTO productDTO = DTOUtils.map(product, ProductDTO.class);
        productDTO.setProductVariantList(
                p.getProductVariantList().stream().map(i -> {
                    i.setProduct(productDTO);
                    return productVariantService.insert(i);
                }).toList());
        return productDTO;
    }

    @Override
    public void update(ProductDTO t) {
        t.getProductVariantList().forEach(i -> {
            i.setProduct(t);
            productVariantService.update(i);
        });
        productRepositories.save(DTOUtils.map(t, Product.class));
    }

    @Override
    public void delete(Integer id) {
        productRepositories.deleteById(id);
    }

    @Override
    public ProductDTO getByUrl(String url) {
        Product product = productRepositories.findByUrl(url).orElseThrow(() -> new IllegalStateException("không tìm thấy sản phẩm có url là " + url));
        ProductDTO productDTO = DTOUtils.map(product, ProductDTO.class);
        return productDTO;
    }
}
