package com.springmvc.services;

import com.springmvc.dtos.OrderDTO;

public interface IOrderService extends ICrudService<OrderDTO> {
}
