package com.springmvc.services;

import com.springmvc.dtos.ProductDTO;
import com.springmvc.entity.Product;

public interface IProductService extends ICrudService<ProductDTO> {
    public ProductDTO getByUrl(String url);
}
