package com.springmvc.services;

import com.springmvc.dtos.RoleDTO;
import com.springmvc.entity.Role;

public interface IRoleService extends ICrudService<RoleDTO> {
}
