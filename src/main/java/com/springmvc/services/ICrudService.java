package com.springmvc.services;

import java.util.List;

public interface ICrudService<T> {
    public List<T> getAll();
    public T getById(Integer id);
    public T insert(T t);
    public void update(T t);
    public void delete(Integer id);
}
