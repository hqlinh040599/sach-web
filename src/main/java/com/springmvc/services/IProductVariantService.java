package com.springmvc.services;

import com.springmvc.dtos.ProductDTO;
import com.springmvc.dtos.ProductVariantDTO;

import java.util.List;

public interface IProductVariantService extends ICrudService<ProductVariantDTO>{
    public ProductVariantDTO getByProductUrl(String url, Integer id);
    public List<ProductVariantDTO> getAllByProduct(ProductDTO productDTO);
    public void deleteByProduct(ProductDTO productDTO);
}
