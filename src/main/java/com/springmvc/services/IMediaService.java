package com.springmvc.services;

import com.springmvc.dtos.MediaDTO;

import java.util.List;

public interface IMediaService extends ICrudService<MediaDTO>{
    public List<MediaDTO> getByTaiKhoanId(int id);
    public List<MediaDTO> getByTaiKhoanVaiTroId(int id);
    public List<MediaDTO> getByName(String name);
}
