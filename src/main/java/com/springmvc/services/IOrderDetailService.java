package com.springmvc.services;

import com.springmvc.dtos.OrderDetailDTO;

public interface IOrderDetailService extends ICrudService<OrderDetailDTO> {
}
