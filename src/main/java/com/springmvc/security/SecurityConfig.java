package com.springmvc.security;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.config.Customizer;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configurers.AbstractHttpConfigurer;
import org.springframework.security.config.annotation.web.configurers.ExceptionHandlingConfigurer;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.security.web.SecurityFilterChain;

@Configuration
@EnableWebSecurity
public class SecurityConfig {

    @Bean
    protected PasswordEncoder passwordEncoder() {
        return new BCryptPasswordEncoder();
    }

    @Bean
    protected CustomAccessDeniedHandler accessDeniedHandler() {
        return new CustomAccessDeniedHandler();
    }

    @Bean
    protected SecurityFilterChain filterChain(HttpSecurity httpSecurity) throws Exception {
        httpSecurity
                .authorizeRequests((a) -> {
                    a.antMatchers("/admin/**").hasRole("ADMIN");
                    a.anyRequest().permitAll();
                })
                .formLogin(f -> {
                    f.loginPage("/login");
                    f.defaultSuccessUrl("/admin/dashboard");
                    f.failureForwardUrl("/login");
                    f.permitAll();
                })
                .csrf(AbstractHttpConfigurer::disable)
                .exceptionHandling((e) -> e.accessDeniedHandler(accessDeniedHandler()))
                .logout((l) -> {
                    l.logoutUrl("/logout");
                    l.logoutSuccessUrl("/login");
                    l.invalidateHttpSession(true);
                    l.deleteCookies("JSESSIONID");
                });
        return httpSecurity.build();
    }
}
