package com.springmvc.entity;

import com.fasterxml.jackson.annotation.*;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;

import javax.persistence.*;
import java.util.*;

@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
@Entity(name = "Users")
public class User implements UserDetails {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;

    @Column(unique = true)
    private String tenTaiKhoan;

    private String matKhau;

    private String hoTen;

    @Column(unique = true)
    private String email;

    private String soDienThoai;
    private Date ngayTao = Calendar.getInstance().getTime();

    @OneToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "idHinhAnh", columnDefinition = "int null")
    private Media avatar;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "idVaiTro", columnDefinition = "int")
    private Role role;

    @OneToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "idDiaChi", columnDefinition = "int null")
    private Address address;

    @Override
    public Collection<? extends GrantedAuthority> getAuthorities() {
        return Arrays.asList(new SimpleGrantedAuthority("ROLE_" + role.getTenVaiTro().toUpperCase()));
    }

    @Override
    public String getPassword() {
        return matKhau;
    }

    @Override
    public String getUsername() {
        return email;
    }

    @Override
    public boolean isAccountNonExpired() {
        return true;
    }

    @Override
    public boolean isAccountNonLocked() {
        return true;
    }

    @Override
    public boolean isCredentialsNonExpired() {
        return true;
    }

    @Override
    public boolean isEnabled() {
        return true;
    }
}
