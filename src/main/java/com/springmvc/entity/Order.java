package com.springmvc.entity;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;
import java.sql.Timestamp;
import java.time.LocalDateTime;
import java.util.Date;
import java.util.List;

@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
@Entity(name = "Orders")
public class Order {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;
    @Column(name = "email", columnDefinition = "varchar(255)")
    private String email;
    @Column(name = "soDienThoai", columnDefinition = "varchar(15)")
    private String soDienThoai;
    @Column(name = "tongTien", columnDefinition = "bigint", nullable = true)
    private long tongTien;
    @Column(name = "ngayTao", columnDefinition = "timestamp")
    private Timestamp ngayTao;

    @OneToMany(cascade = CascadeType.REMOVE, mappedBy = "order")
    private List<OrderDetail> orderDetailList;
}
