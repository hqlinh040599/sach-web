package com.springmvc.entity;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.hibernate.annotations.ColumnDefault;

import javax.persistence.*;
import java.util.List;

@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
@Entity(name = "Products")
public class Product {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;
    @Column(columnDefinition = "nvarchar(128)")
    private String tenSanPham;
    @Column(columnDefinition = "nvarchar(1000)")
    private String moTaNgan;
    @Column(columnDefinition = "bit", nullable = true)
    private Boolean trangThai;
    @Column(columnDefinition = "nvarchar(255)", unique = true)
    private String url;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "idHinhAnh")
    private Media hinhAnh;

    @OneToMany(cascade = CascadeType.REMOVE, mappedBy = "product")
    private List<ProductVariant> productVariantList;
}
