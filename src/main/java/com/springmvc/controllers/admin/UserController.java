package com.springmvc.controllers.admin;

import com.springmvc.dtos.UserDTO;
import com.springmvc.entity.User;
import com.springmvc.services.IUserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

@Controller
public class UserController {
    @Autowired
    private IUserService userService;

    @RequestMapping(path = "/login", method = RequestMethod.GET)
    public String getLoginPage(HttpServletRequest request, Model model)
    {
        HttpSession session = request.getSession();
        String accessDeniedError = (String) session.getAttribute("accessDeniedError");
        session.removeAttribute("accessDeniedError");
        model.addAttribute("accessDeniedError", accessDeniedError);
        return "login/login.jsp";
    }

    @RequestMapping(path = "/register", method = RequestMethod.GET)
    public String getRegisterPage()
    {
        return "register/register.jsp";
    }

    @RequestMapping(path = "/register", method = RequestMethod.POST)
    public String register(@ModelAttribute UserDTO user)
    {
        userService.insert(user);
        return "redirect:login";
    }

    @RequestMapping(path = "/admin/users", method = RequestMethod.GET)
    public String getAll()
    {
        return "/admin/user/list-users.jsp";
    }
}
