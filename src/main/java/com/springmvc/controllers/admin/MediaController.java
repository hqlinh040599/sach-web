package com.springmvc.controllers.admin;

import com.springmvc.services.IMediaService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

@Controller
@RequestMapping(path = "/admin")
public class MediaController {
    @Autowired
    private IMediaService imageService;

    @RequestMapping(path = "/medias", method = RequestMethod.GET)
    public String getMediaPage() {
        return "/admin/media.jsp";
    }
}
