package com.springmvc.controllers.admin;

import org.springframework.security.core.Authentication;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

import java.security.Principal;

@Controller(value = "adminHomeController")
@RequestMapping(path = "/admin")
public class HomeController {
    @RequestMapping(path = {"", "/", "/dashboard"}, method = RequestMethod.GET)
    public String getHome() {
        return "/admin/dashboard.jsp";
    }
}
