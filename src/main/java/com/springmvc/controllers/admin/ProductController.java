package com.springmvc.controllers.admin;

import com.springmvc.services.IProductService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.stereotype.Controller;
import org.springframework.stereotype.Repository;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

import java.security.Principal;

@Controller
@RequestMapping(path = "/admin")
public class ProductController {
    @Autowired
    private IProductService productService;

    @RequestMapping(path = "/products", method = RequestMethod.GET)
    public String getProductPage(Model model) {
        model.addAttribute("products", productService.getAll());
        return "/admin/product/list-products.jsp";
    }

    @RequestMapping(path = "/product/add-new", method = RequestMethod.GET)
    public String getAddNewPage(Model model) {
        model.addAttribute("title", "Add new");
        return "/admin/product/product.jsp";
    }

    @RequestMapping(path = "/product/{url}", method = RequestMethod.GET)
    public String getUpdatePage(@PathVariable("url") String url, Model model) {
        model.addAttribute("title", "Update");
        return "/admin/product/product.jsp";
    }
}
