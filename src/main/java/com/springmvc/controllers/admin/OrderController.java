package com.springmvc.controllers.admin;

import com.springmvc.services.IOrderService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

@Controller
@RequestMapping(path = "/admin")
public class OrderController {
    @Autowired
    private IOrderService orderService;

    @RequestMapping(path = "/orders", method = RequestMethod.GET)
    public String getProductPage(Model model) {
        model.addAttribute("orders", orderService.getAll());
        return "/admin/order/list-orders.jsp";
    }

    @RequestMapping(path = "/order/{id}", method = RequestMethod.GET)
    public String getUpdatePage(@PathVariable("id") String id, Model model) {
        model.addAttribute("title", "Update");
        return "/admin/order/order.jsp";
    }
}
