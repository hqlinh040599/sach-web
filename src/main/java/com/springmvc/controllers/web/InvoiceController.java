package com.springmvc.controllers.web;

import com.springmvc.dtos.MediaDTO;
import com.springmvc.dtos.OrderDTO;
import com.springmvc.services.IOrderService;
import com.springmvc.services.IProductService;
import com.springmvc.services.IProductVariantService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.multipart.MultipartFile;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.StandardCopyOption;

@Controller
@RequestMapping
public class InvoiceController {
    private @Autowired IOrderService orderService;
    @RequestMapping(path = "/checkout", method = RequestMethod.GET)
    public String getCheckout() {
        return "/web/checkout.jsp";
    }

    @RequestMapping(path = "/orderSummaries", method = RequestMethod.GET)
    public String getOrderSummaries(@RequestParam("variant") int id, Model model) {
        model.addAttribute("order", orderService.getById(id));
        return "/web/orderSummaries.jsp";
    }
}
