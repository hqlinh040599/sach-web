package com.springmvc.controllers.web;

import com.springmvc.services.IProductService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

@Controller(value = "webHomeController")
public class HomeController {
    @Autowired
    private IProductService productService;

    @RequestMapping(path = "/trang-chu", method = RequestMethod.GET)
    public String getHome(Model model) {
        model.addAttribute("products", productService.getAll());
        return "/web/home.jsp";
    }
}
