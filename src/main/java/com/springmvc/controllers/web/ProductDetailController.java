package com.springmvc.controllers.web;

import com.springmvc.services.IProductService;
import com.springmvc.services.IProductVariantService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

@Controller
@RequestMapping(path = "/product")
public class ProductDetailController {

    private @Autowired IProductVariantService productVariantService;

    @RequestMapping(path = "/{url}")
    public String getByPath(@RequestParam("variant") int id, Model model) {
        model.addAttribute("productDetail", productVariantService.getById(id));
        return "/web/single-page-product.jsp";
    }
}
