package com.springmvc.utils;

public record ResponseObject<T>(T data, String message, boolean status) { }
