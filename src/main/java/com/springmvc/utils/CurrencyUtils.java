package com.springmvc.utils;

import java.text.DecimalFormat;

public class CurrencyUtils {
    private static final DecimalFormat decimalFormat = new DecimalFormat("#,###");

    public static String parseString(long price) {
        return decimalFormat.format(price).replace(",", ".");
    }

    public static Long parseLong(String price) {
        return Long.parseLong(price.replace(".", ""));
    }
}
