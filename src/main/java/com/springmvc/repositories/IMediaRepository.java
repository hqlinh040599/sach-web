package com.springmvc.repositories;

import com.springmvc.entity.Media;
import com.springmvc.entity.Role;
import com.springmvc.entity.User;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.repository.query.Param;

import java.util.List;

public interface IMediaRepository extends JpaRepository<Media, Integer> {
    public List<Media> findByUser(@Param("user") User user);
    public List<Media> findByUserRole(@Param("role") Role role);
    public List<Media> findByName(@Param("name") String name);
    public boolean existsAllByName(@Param("name") String name);
}
