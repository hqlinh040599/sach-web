package com.springmvc.repositories;

import com.springmvc.entity.Product;
import com.springmvc.entity.ProductVariant;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.repository.query.Param;

import java.util.List;
import java.util.Optional;

public interface IProductVariantRepository extends JpaRepository<ProductVariant, Integer> {
    public List<ProductVariant> findAllByProduct(@Param("product") Product product);
    public ProductVariant findById(@Param("id") int id);
    public void deleteAllByProduct(@Param("product") Product product);
}
