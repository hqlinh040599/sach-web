package com.springmvc.repositories;

import com.springmvc.entity.Product;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.transaction.annotation.Transactional;

import java.util.Optional;

public interface IProductRepository extends JpaRepository<Product, Integer> {
    public Optional<Product> findByUrl(@Param("url") String url);

}
