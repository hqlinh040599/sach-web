package com.springmvc.repositories;

import com.springmvc.entity.User;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.repository.query.Param;

import java.util.Optional;

public interface IUserRepository extends JpaRepository<User, Integer> {
    public Optional<User> findByEmail(@Param("email") String email);
    public Optional<User> findByTenTaiKhoan(@Param("username") String username);
}
