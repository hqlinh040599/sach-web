package com.springmvc.repositories;

import com.springmvc.entity.Order;
import com.springmvc.entity.OrderDetail;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.repository.query.Param;

import java.util.List;

public interface IOrderDetailRepository extends JpaRepository<OrderDetail, Integer> {
    public List<OrderDetail> findAllByOrder(@Param("order") Order order);
}
