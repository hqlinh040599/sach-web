public interface ICrudTest<T> {
    public void getAll();
    public void getById() throws Exception;
    public void insert();
    public void update();
    public void delete();
}
